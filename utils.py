def get_ips(ip_prefix, ip_suffix):
  ipv4 = f"{ip_prefix[0][0]}{ip_suffix}/{ip_prefix[0][1]}"
  ipv6 = f"{ip_prefix[1][0]}{ip_suffix}/{ip_prefix[1][1]}"

  return [ipv4, ipv6]

def get_pure_ips(ip_prefix, ip_suffix):
  ipv4 = f"{ip_prefix[0][0]}{ip_suffix}"
  ipv6 = f"{ip_prefix[1][0]}{ip_suffix}"

  return [ipv4, ipv6]