import pickle
import ast
from Crypto.Random import get_random_bytes
from getpass import getpass

from encrypt import *

# import existing keys from txt file dicts
with open("import/privkeys_server.import", "r") as data:
    privkeys_server = ast.literal_eval(data.read())
with open("import/privkeys_clients.import", "r") as data:
    privkeys_clients = ast.literal_eval(data.read())
with open("import/privkeys_restricted.import", "r") as data:
    privkeys_restricted = ast.literal_eval(data.read())

with open("import/pubkeys_server.import", "r") as data:
    pubkeys_server = ast.literal_eval(data.read())
with open("import/pubkeys_clients.import", "r") as data:
    pubkeys_clients = ast.literal_eval(data.read())
with open("import/pubkeys_restricted.import", "r") as data:
    pubkeys_restricted = ast.literal_eval(data.read())

# generate a salt, passed to files
salt = get_random_bytes(32)
# create a password
password = getpass()
# derive the encrytion key
enc_key = get_key(password, salt)

pubkeys = [pubkeys_server, pubkeys_clients, pubkeys_restricted]
privkeys = [privkeys_server, privkeys_clients, privkeys_restricted]

# pickle dicts, private keys encrypted
write_dicts([pubkeys, privkeys], salt, password)

"""
[salt, password, [pubkeys, privkeys]] = read_dicts()

for pub in pubkeys:
  for ip in pub:
    print(ip,pub[ip])
"""