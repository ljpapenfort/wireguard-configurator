import pickle
import os
import sys

from utils import *
from config import *
from encrypt import *

# load data
[salt, password, [pubkeys_all, privkeys_all]] = read_dicts()

[pubkeys_server, pubkeys_clients, pubkeys_restricted] = pubkeys_all
[privkeys_server, privkeys_clients, privkeys_restricted] = privkeys_all

# write a peer to the server config
def write_peer(item,conf,subnet):
  ip,[name,key] = item
  [ipv4, ipv6] = get_pure_ips(subnet,ip)

  conf.write(f"# {name}\n")
  conf.write("[Peer]\n")
  conf.write(f"PublicKey = {key}\n")
  conf.write(f"AllowedIPs = {ipv4}/32, {ipv6}/128\n")
  conf.write("\n")

# write a host to the hosts file
def write_host(item,conf,server,subnet):
  ip,[name,key] = item
  [ipv4, ipv6] = get_pure_ips(subnet,ip)

  if server == central_server or subnet == server_subnet:
    conf.write(f"{ipv4} {name}.{domain} {name}\n")
    conf.write(f"{ipv6} {name}.{domain} {name}\n")
  else:
    conf.write(f"{ipv4} {name}.{server}.{domain}\n")
    conf.write(f"{ipv6} {name}.{server}.{domain}\n")
  conf.write("\n")

# get everything easily by name
named_ips = {v[0]: k for k, v in pubkeys_server.items()}
named_pubkeys = {v[0]: v[1] for k, v in pubkeys_server.items()}
named_privkeys = {v[0]: v[1] for k, v in privkeys_server.items()}

# getting the hostname for which the config should be generated
name = sys.argv[1]
ip = named_ips[name]

if len(sys.argv) >= 3:
  out_dir = sys.argv[2]
else:
  out_dir = name

os.makedirs(os.path.join(out_dir,"wireguard"), exist_ok=True)

privkey = privkeys_server[ip][1]
replacements = {
  "@NAME@": name,
  "@DOMAIN@": domain,
  "@PRIVKEYSERVER@": privkey,
}

# common config generation for client peers
interfaces = ["forward", "restricted"]
peers = [pubkeys_clients, pubkeys_restricted]
subnets = [forward_subnet, restricted_subnet]

for k,interface in enumerate(interfaces):
  # generate server config
  with open(os.path.join(out_dir,"wireguard",f"wg_{interface}.conf"),"w") as conf:
    with open("templates/template_server.conf","r") as template:

      [ipv4, ipv6] = get_ips(subnets[k][ip], 1)
      replacements["@IPS@"] = f"{ipv4}, {ipv6}"

      replacements["@SRVPORT@"] = f"{server_ports[interface]}"

      # write base config
      for line in template:
        for i,j in replacements.items():
          line = line.replace(i,j)
        conf.write(line)

      # write peers
      for item in peers[k].items():
        write_peer(item,conf,subnets[k][ip])

[ipv4, ipv6] = get_ips(server_subnet, ip)
replacements["@IPS@"] = f"{ipv4}, {ipv6}"

# now discriminate between gateway and servers
if ip == 1:
  # generate server hosts file for gateway, serving DNS
  with open(os.path.join(out_dir,"hosts"),"w") as hosts:
    with open("templates/template_hosts","r") as template:

      # write base config
      for line in template:
        for i,j in replacements.items():
          line = line.replace(i,j)
        hosts.write(line)

      # include all server connections
      for item in pubkeys_server.items():
        write_host(item,hosts,pubkeys_server[1][0],server_subnet)

      # loop over all wireguard servers and include their peers
      for srv_ip in pubkeys_server:
        srv_name = pubkeys_server[srv_ip][0]

        for item in pubkeys_clients.items():
          write_host(item,hosts,srv_name,forward_subnet[srv_ip])

        for item in pubkeys_restricted.items():
          write_host(item,hosts,srv_name,restricted_subnet[srv_ip])

  # generate routes script
  with open(os.path.join(out_dir,"wireguard","routes.enable"),"w") as conf:
    conf.write("#!/bin/bash\n")

    # loop over all wireguard servers, except the gateway
    # to generate the appropriate routes
    for srv_ip in sorted(pubkeys_server.keys())[1:]:
      [ipv4_forward, ipv6_forward] = get_ips(forward_subnet[srv_ip], 0)
      [ipv4_restricted, ipv6_restricted] = get_ips(restricted_subnet[srv_ip], 0)

      [ipv4_all, ipv6_all] = get_ips(all_subnet, 0)
      [ipv4_srv, ipv6_srv] = get_pure_ips(server_subnet, srv_ip)

      rules = [f"ip route add {ipv4_forward} via {ipv4_srv} dev wg_gw\n",
               f"ip -6 route add {ipv6_forward} via {ipv6_srv} dev wg_gw\n",
               f"ip route add {ipv4_restricted} via {ipv4_srv} dev wg_gw\n",
               f"ip -6 route add {ipv6_restricted} via {ipv6_srv} dev wg_gw\n",
               f"wg set wg_gw peer {pubkeys_server[srv_ip][1]} allowed-ips {ipv4_all},{ipv6_all}\n"]

      # generate routes config
      conf.writelines(rules)

else:
  [ipv4_forward, ipv6_forward] = get_ips(forward_subnet[ip], 0)
  [ipv4_restricted, ipv6_restricted] = get_ips(restricted_subnet[ip], 0)

  [ipv4_all, ipv6_all] = get_ips(all_subnet, 0)

  rules = [f"echo '1 wireguard' >> /etc/iproute2/rt_tables.d/wireguard.conf\n",

           f"ip route add 0.0.0.0/0 dev wg_gw table wireguard\n",

           f"ip rule add from {ipv4_forward} lookup wireguard\n",
           f"ip rule add from {ipv4_restricted} lookup wireguard\n",

           f"ip -6 route add ::/0 dev wg_gw table wireguard\n",

           f"ip -6 rule add from {ipv6_forward} lookup wireguard\n",
           f"ip -6 rule add from {ipv6_restricted} lookup wireguard\n",

           f"ip route add {ipv4_forward} dev wg_forward table wireguard\n",
           f"ip -6 route add {ipv6_forward} dev wg_forward table wireguard\n",

           f"ip route add {ipv4_restricted} dev wg_restricted table wireguard\n",
           f"ip -6 route add {ipv6_restricted} dev wg_restricted table wireguard\n",

           f"wg set wg_gw peer {pubkeys_server[1][1]} allowed-ips 0.0.0.0/0,::/0\n"]

  # generate PostUp
  with open(os.path.join(out_dir,"wireguard","routes.enable"),"w") as conf:
    conf.write("#!/bin/bash\n")
    conf.writelines(rules)

  rules = [f"ip route del {ipv4_forward} dev wg_forward table wireguard\n",
           f"ip -6 route del {ipv6_forward} dev wg_forward table wireguard\n",

           f"ip route del {ipv4_restricted} dev wg_restricted table wireguard\n",
           f"ip -6 route del {ipv6_restricted} dev wg_restricted table wireguard\n",

           f"ip rule del from {ipv4_forward} lookup wireguard\n",
           f"ip rule del from {ipv4_restricted} lookup wireguard\n",

           f"ip -6 rule del from {ipv6_forward} lookup wireguard\n",
           f"ip -6 rule del from {ipv6_restricted} lookup wireguard\n",

           f"rm /etc/iproute2/rt_tables.d/wireguard.conf\n"]

  # generate PostDown
  with open(os.path.join(out_dir,"wireguard","routes.disable"),"w") as conf:
    conf.write("#!/bin/bash\n")
    conf.writelines(rules)

if ip == 1:
  replacements["#PostUp"] = "PostUp"
else:
  replacements["#PostUp"] = "PostUp"
  replacements["#PostDown"] = "PostDown"

replacements["#DNS"] = "DNS"

# depending if we got the central server, generate different gw connection configs
if pubkeys_server[ip][0] == central_server:

  with open(os.path.join(out_dir,"wireguard",f"wg_gw.conf"),"w") as conf:
    with open("templates/template_server.conf","r") as template:
      replacements["@SRVPORT@"] = f"{server_ports['gw']}"
      replacements["@DNS@"] = f"{server_subnet[0][0]}1, {server_subnet[1][0]}1"

      # write base config
      for line in template:
        for i,j in replacements.items():
          line = line.replace(i,j)
        conf.write(line)

      for item in pubkeys_server.items():
        ip,[name,pubkey] = item
        if name != central_server:
          write_peer(item,conf,server_subnet)

else:
  # FIXME this doesn't work on the gw since the hostname get's overwritten in the hosts file
  endpoint_pubkey = named_pubkeys[central_server]
#  endpoint = f"{central_server}.{domain}:{server_ports['gw']}"
  endpoint = f"[{central_ipv6}]:{server_ports['gw']}"

  # DNS runs on the gateway
  replacements["@DNS@"] = f"{server_subnet[0][0]}1, {server_subnet[1][0]}1"
  replacements["@NETS@"] = f"{server_subnet[0][0]}0/{server_subnet[0][1]}, {server_subnet[1][0]}0/{server_subnet[1][1]}"
  replacements["@PRIVKEY@"] = privkey
  replacements["@PUBKEYSERVER@"] = endpoint_pubkey
  replacements["@ENDPOINT@"] = endpoint

  # write gw peer config
  with open(os.path.join(out_dir,"wireguard",f"wg_gw.conf"),"w") as conf:
    with open("templates/template_restricted.conf","r") as template:
      for line in template:
        for i,j in replacements.items():
          line = line.replace(i,j)
        conf.write(line)
