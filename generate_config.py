import os
import sys
import shutil
import subprocess
from subprocess import Popen
from subprocess import PIPE

from config import *
from encrypt import *
from utils import *

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('names', nargs='+', help='account names')
parser.add_argument("-r","--restricted", help="restricted client",action="store_true")
parser.add_argument("-o", "--dir", default='./', help="output directory")
args = parser.parse_args()

names = args.names
out_dir = args.dir
res = args.restricted

for name in names:
  # load data
  [salt, password, [pubkeys_all, privkeys_all]] = read_dicts()
  
  [pubkeys_server, pubkeys_clients, pubkeys_restricted] = pubkeys_all
  [privkeys_server, privkeys_clients, privkeys_restricted] = privkeys_all
  
  # check if hostname is already taken
  for pubs in pubkeys_all:
    for item in pubs.items():
      key,val = item
      if val[0] == name:
        sys.exit(f"Got {name} already in dict leading to hostname conflicts, stopping.")
  
  # generate private and public key
  privkey = subprocess.run(["wg","genkey"], stdout=PIPE, stderr=PIPE).stdout[:-1].decode()
  
  p1 = Popen(["echo",privkey], stdout=PIPE)
  p2 = Popen(["wg", "pubkey"], stdin=p1.stdout, stdout=PIPE)
  p1.stdout.close()
  
  pubkey = p2.communicate()[0][:-1].decode()
  
  if res:
    print(f"Generating restricted client configs for {name}")
    pubkeys = pubkeys_restricted
    privkeys = privkeys_restricted
    server_port = server_ports['restricted']
    template_file = "templates/template_restricted.conf"
  else:
    print(f"Generating forwarding client configs for {name}")
    pubkeys = pubkeys_clients
    privkeys = privkeys_clients
    server_port = server_ports['forward']
    template_file = "templates/template_forward.conf"
  
  # get next ip avail
  ip_list = pubkeys.keys()
  ip_avail = [i for i in range(2,255) if i not in ip_list]
  ip = sorted(ip_avail)[0]
  
  pubkeys[ip] = [name,pubkey]
  privkeys[ip] = [name,privkey]
  
  """
  for ip in pubkeys_all[1]:
    print(ip, pubkeys_all[1][ip])
  for ip in pubkeys_all[2]:
    print(ip, pubkeys_all[2][ip])
  """
  
  # write dictionaries to file
  write_dicts([pubkeys_all, privkeys_all], salt, password)
  
  # loop over all wireguard servers, except the gateway
  for srv_ip in sorted(pubkeys_server)[1:]:
  #for srv_ip in pubkeys_server:
    # client with or without forwarding?
    if res:
      subnet = restricted_subnet[srv_ip]
      print(f"Using restricted subnet {subnet}")
    else:
      subnet = forward_subnet[srv_ip]
      print(f"Using forwarding subnet {subnet}")
  
    # get server hostname and pubkey
    srv_name, srv_pubkey = pubkeys_server[srv_ip]
    endpoint = f"{srv_name}.{domain}:{server_port}"
  
    print(f"For wireguard server {srv_name}")
  
    [ipv4, ipv6] = get_ips(subnet, ip)
  
    print(f"Client IPs: {ipv4} and {ipv6}")
  
    os.makedirs(name, exist_ok=True)
  
    replacements = {
      "@IPS@": f"{ipv4}, {ipv6}",
      "@DNS@": f"{server_subnet[0][0]}1, {server_subnet[1][0]}1",
      "@NETS@": f"{all_subnet[0][0]}0/{all_subnet[0][1]}, {all_subnet[1][0]}0/{all_subnet[1][1]}",
      "@PRIVKEY@": privkey,
      "@PUBKEYSERVER@": srv_pubkey,
      "@ENDPOINT@": endpoint,
    }
  
    with open(os.path.join(name,f"wg_{srv_name}.conf"),"w") as conf:
      with open(template_file,"r") as template:
        for line in template:
          for i,j in replacements.items():
            line = line.replace(i,j)
          conf.write(line)
  
    with open(os.path.join(name,f"wg_{srv_name}.conf"),"r") as conf:
      subprocess.run(["qrencode","-o",os.path.join(name,f"wg_{srv_name}.png")], stdin=conf)
  
  if config_password != "":
    subprocess.run(["7z", "a", f"{name}.zip", f"{name}", f"-p{config_password}"])
    shutil.rmtree(f"{name}")
    shutil.move(f"{name}.zip", os.path.join(out_dir,f"{name}.zip"))
  else:
    shutil.move(f"{name}", os.path.join(out_dir,f"{name}"))