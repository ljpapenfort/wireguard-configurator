import os
import pickle
from Cryptodome.Cipher import AES
from Cryptodome.Protocol.KDF import PBKDF2
from getpass import getpass

from config import *

def get_key(pw, salt):
  return PBKDF2(pw, salt, dkLen=32)

def encrypt(entry, enc_key):
  name, key = entry
  key = key.encode('ascii')

  cipher = AES.new(enc_key, AES.MODE_EAX)
  enc_data, tag = cipher.encrypt_and_digest(key)
  nonce = cipher.nonce

  return [name, [nonce, tag, enc_data]]

def decrypt(entry, enc_key):
  name, [nonce, tag, enc_data] = entry

  decipher = AES.new(enc_key, AES.MODE_EAX, nonce)
  key = decipher.decrypt_and_verify(enc_data, tag)
  key = key.decode('ascii')

  return [name, key]

def write_dicts(dicts, salt, password):
  with open(os.path.join(dicts_path,"wireguard.dicts"),"wb") as wgfile:
    wgfile.write(salt)

    [pubkeys, privkeys] = dicts

    enc_key = get_key(password, salt)

    for priv in privkeys:
      for ip in priv:
        priv[ip] = encrypt(priv[ip], enc_key)

    pickle.dump(pubkeys, wgfile)
    pickle.dump(privkeys, wgfile)

def read_dicts():
  with open(os.path.join(dicts_path,"wireguard.dicts"),"rb") as wgfile:
    salt = wgfile.read(32)

    pubkeys = pickle.load(wgfile)
    privkeys = pickle.load(wgfile)

    password = getpass()
    enc_key = get_key(password, salt)

    for priv in privkeys:
      for ip in priv:
        priv[ip] = decrypt(priv[ip], enc_key)

    return [salt, password, [pubkeys, privkeys]]