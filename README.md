Wirguard config (re)generator for a VPN with multiple access points and subnets.
More information soon, but the scripts are quite self-explanatory.

Expects one server to act as a forwarding gateway to the internet as well as DNS server.

This doesn't include firefall, forwarding or NAT rules and have to be
added manually (maybe I will add some based on FireHol at some point).
But I generates (hopefully correct) routing tables and rules.