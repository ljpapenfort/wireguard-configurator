import os
import sys
import subprocess
from subprocess import Popen
from subprocess import PIPE
import shutil

from utils import *
from config import *
from encrypt import *

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("name")
parser.add_argument("-r","--restricted", help="restricted client",action="store_true")
args = parser.parse_args()

name = args.name
res = args.restricted

# load data
[salt, password, [pubkeys_all, privkeys_all]] = read_dicts()

[pubkeys_server, pubkeys_clients, pubkeys_restricted] = pubkeys_all
[privkeys_server, privkeys_clients, privkeys_restricted] = privkeys_all

# loop over all wireguard servers, except the gateway
for srv_ip in sorted(pubkeys_server)[1:]:
#for srv_ip in pubkeys_server:
  # client with or without forwarding?
  if res:
    pubkeys = pubkeys_restricted
    privkeys = privkeys_restricted
    subnet = restricted_subnet[srv_ip]
    server_port = server_ports['restricted']
    template = "templates/template_restricted.conf"
  else:
    pubkeys = pubkeys_clients
    privkeys = privkeys_clients
    subnet = forward_subnet[srv_ip]
    server_port = server_ports['forward']
    template = "templates/template_forward.conf"

  # mapping to names instead of IP suffix
  named_ips = {v[0]: k for k, v in pubkeys.items()}
  named_pubkeys = {v[0]: v[1] for k, v in pubkeys.items()}
  named_privkeys = {v[0]: v[1] for k, v in privkeys.items()}

  if name not in named_ips:
    sys.exit(f"{name} not found in {named_ips.keys()}")

  os.makedirs(name, exist_ok=True)

  # get ip suffix and private key
  ip = named_ips[name]
  privkey = named_privkeys[name]

  # wireguard server hostname and pubkey
  srv_name, srv_pubkey = pubkeys_server[srv_ip]
  endpoint = f"{srv_name}.{domain}:{server_port}"

  # recreate IPs from subnet and suffix
  [ipv4, ipv6] = get_ips(subnet, ip)

  replacements = {
    "@IPS@": f"{ipv4}, {ipv6}",
    # DNS runs on the gateway
    "@DNS@": f"{server_subnet[0][0]}1, {server_subnet[1][0]}1",
    # allow all available local subnets if default forwarding is disabled
    "@NETS@": f"{all_subnet[0][0]}0/{all_subnet[0][1]}, {all_subnet[1][0]}0/{all_subnet[1][1]}",
    "@PRIVKEY@": privkey,
    "@PUBKEYSERVER@": srv_pubkey,
    "@ENDPOINT@": endpoint,
  }

  # write config file
  with open(os.path.join(name,f"wg_{srv_name}.conf"),"w") as conf:
    with open(template,"r") as template:
      for line in template:
        for i,j in replacements.items():
          line = line.replace(i,j)
        conf.write(line)

  # create a QR code
  with open(os.path.join(name,f"wg_{srv_name}.conf"),"r") as conf:
    subprocess.run(["qrencode","-o",f"{name}/wg_{srv_name}.png"], stdin=conf)

if config_password != "":
  subprocess.run(["7z", "a" ,f"{name}.zip",f"{name}",f"-p{config_password}"])
  shutil.rmtree(f"{name}")